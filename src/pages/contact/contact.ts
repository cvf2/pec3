import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';


@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

  constructor(public navCtrl: NavController, public alertCtrl: AlertController) {


  }

  showCheckbox() {
    let alert = this.alertCtrl.create({
      title: 'Bienvenido!',
      subTitle: 'Esto es una template con tabs y enlazamos vista con controlador.',
      buttons: ['OK']
    });
    alert.present();
  }



}
